#!/usr/bin/python

"Usage: %s <server> <port> <word>"

import socket, sys

if len(sys.argv) != 4:
	print __doc__ % sys.argv[0]
	sys.exit()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((sys.argv[1], int(sys.argv[2])))
sock.send(sys.argv[3])
echo = sock.recv(256)
print "Received from:", sock.getpeername()
print echo
sock.close()
