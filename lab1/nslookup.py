#!/usr/bin/python


"Usage: %s <hostname>"

import socket, sys

if len(sys.argv) != 2:
    print __doc__ % sys.argv[0]
    sys.exit()

print socket.gethostbyname(sys.argv[1])

