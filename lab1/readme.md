#CA645 Network Security Lab 1 20141. Take a copy of nslookup. py.	
	a. Study the code and run it.	
	b. What does this program do?	
	c. Use strace to list the network system calls the program makes.
	```	
strace -e trace=network python nslookup.py johnnynet.homeip.net
``````socket(PF_FILE, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 3connect(3, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT 
(No such file or directory) local nameserver lookupsocket(PF_FILE, SOCK_STREAM|SOCK_CLOEXEC|SOCK_NONBLOCK, 0) = 3connect(3, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT (No such file or directory)socket(PF_INET, SOCK_DGRAM|SOCK_NONBLOCK, IPPROTO_IP) = 3connect(3, {sa_family=AF_INET, sin_port=htons(53), sin_addr=inet_addr("127.0.0.1")}, 16) = 0send(3, "\27\220\1\0\0\1\0\0\0\0\0\0\tjohnnynet\6homeip\3ne"..., 38, MSG_NOSIGNAL) = 38recvfrom(3, "\27\220\201\200\0\1\0\1\0\4\0\6\tjohnnynet\6homeip\3ne"..., 1024, 0, {sa_family=AF_INET, sin_port=htons(53), sin_addr=inet_addr("127.0.0.1")}, [16]) = 256109.255.131.68```d. What kind of socket(s) does the program create?

```
SOCK_DGRAM, IPPROTO_IP```
e. To what host / IP address : port does the program send its queries? port=htons(53)With what service is this “well-known” port number associated? 

```
DNS```
f. Does the program establish a connection with the server? What does it mean to call connect on a SOCK_DGRAM socket? 

```
UDP
```g. What application protocol does the program use? 

```
DNS```
h. What transport protocol does the program use? 

```
UDP
```i. What network protocol does the program use? 
```
IP```
### 2. Take copies of udpserver . py and udpclient . py.a. Study the code and run it.
b. What do these programs do?
c. Use netstat to list all UDP listening sockets. Identify the above server among netstat’s output.```netstat -l -n -p --udp netstat -a -p UDP | grep udp (Not all processes could be identified, non-owned process info will not be shown, you would have to be root to see it all.)udp        0      0 localhost:domain        *:*                                 -               udp        0      0 *:bootpc                *:*                                 -               udp        0      0 *:44174                 *:*                                 -               udp        0      0 localhost:2222          *:*                                 6373/python     udp        0      0 *:mdns                  *:*                                 -               udp6       0      0 [::]:34974              [::]:*                              -               udp6       0      0 [::]:mdns               [::]:*                              -               ```d. Note how the servers you listed above listen on different interfaces. Examine the effect on netstat's output of listening on the following interfaces in the server:
- ○ '127.0.0.1' vs.- ○ 'localhost' vs.- ○ '' (the empty string) vs.- ○ 'your IP address' (in dotted-decimal)e. What is the difference between the various interfaces?f. Use netstat to identify all of the interfaces on your machine and their IP addresses.3. Take copies of tcpserver . py and tcpclient . py.a. Study the code and run it. 

```
python tcpclient.py localhost 2345 hi```b. What do these programs do?c. Use netstat to list all TCP listening sockets. Identify the above server among netstat’s output.d. Why does netstat show a connection state for TCP but not UDP?e. What is the newsock used in the TCP server but not in the UDP server?f. What states may netstat show a TCP connection occupying during its lifetime? (The netstat man page will tell you.)g. Can you see any obvious limitations in this TCP server? How would a serious TCP server overcome this limitation?h. What does this line do:sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)i. What does this line do:sock.listen(5)j. Can you have a UDP server and TCP server listening on the same interface and on the same port? If so, what uniquely identifies a communication endpoint?###4. Take a copy of server . py.a. Study the code and run it.b. What does this program do?c. Test it by connecting to it from two netcat clients.```
python tcpclient.py localhost 2345 hi```d. List established connections using netstat. 

```
netstat -t```e. This program uses a particular system call to monitor a list of sockets for I/O activity. What is that system call?f. Try running the server and netcat client(s) on separate lab machines. Can they communicate? Is a firewall blocking such communication?g. Try running the server and client(s) on separate subnets e.g. run the server on a lab machine and clients on the wireless network. Can they communicate? What are your thoughts on the security of this arrangement?