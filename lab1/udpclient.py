#!/usr/bin/python

"Usage: %s <server> <port> <word>"

import socket, sys

if len(sys.argv) != 4:
    print __doc__ % sys.argv[0]
    sys.exit()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(sys.argv[3], (sys.argv[1], int(sys.argv[2])))

echo, server = sock.recvfrom(256)

print "Received from:", server
print echo

sock.close()

