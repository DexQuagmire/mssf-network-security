#!/usr/bin/python

"Usage: %s <port>"

import socket, sys

if len(sys.argv) != 2:
    print __doc__ % sys.argv[0]
    sys.exit()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

sock.bind(('localhost', int(sys.argv[1])))

while True:

    message, client = sock.recvfrom(256)

    print "Received from:", client
    print message

    sock.sendto(message, client)

