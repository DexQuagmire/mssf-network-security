#!/usr/bin/python

"Usage: %s <port>"

import socket, sys

if len(sys.argv) != 2:
	print __doc__ % sys.argv[0]
	sys.exit()

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)

sock.bind(('localhost', int(sys.argv[1])))

sock.listen(5)

while True:
	newsock, client = sock.accept()
	message = newsock.recv(256)
	print "Received from:", client
	print message
	newsock.send(message)
	newsock.close()
