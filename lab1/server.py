#!/usr/bin/python

"Usage: %s <port>"

import socket, select, sys

class Server:

    def __init__(self, port):
        self.port = port;
        self.srvsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.srvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        self.srvsock.bind(('', port))
        self.srvsock.listen(5)
        self.descriptors = [self.srvsock]

    def run(self):

        while True:

            (r, w, e) = select.select(self.descriptors, [], [])
            for sock in r:
                if sock == self.srvsock:
                    self.accept_new_connection()
                    continue

                str = sock.recv(256)
                client = sock.getpeername()
                if not str:
                    str = "%s just left\n" % (client,)
                    self.descriptors.remove(sock)
                    sock.close()
                else:
                    str = "%s says %s" % (client, str)
 
                self.broadcast_string(str, sock)


    def broadcast_string(self, str, omit):
        for sock in self.descriptors:
            if (sock == self.srvsock or sock == omit):
                continue
            sock.send(str)

    def accept_new_connection(self):
        newsock, client = self.srvsock.accept()
        str = "Welcome %s to the discussion\n" % (client,)
        self.descriptors.append(newsock)
        self.broadcast_string(str, newsock)


if len(sys.argv) != 2:
    print __doc__ % sys.argv[0]
    sys.exit()

cs = Server(int(sys.argv[1])).run()

