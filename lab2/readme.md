#CA645 Network Security Lab 2 2014

For many of these exercises you need to work in pairs. Boot one machine into 
Windows. Download the Fedora Security Spin VM to C:\Temp. Unpack it. Run it in 
VMWare Workstation/Player. Usernames (and passwords) and student and root. 
(Note that future users can delete this VM so save anything you want to keep in 
your own account.)

1. Use the route and/or netstat and/or ip command line utilities to look up 
your routing table. What is the network class of your LAN? **CLASS C** What is your default **GATEWAY** route? How does your machine determine these routes when it boots? Is the order of these routes in the table important?**NO**

``` bash
netstat -nr
```

2. If you have a laptop, repeat the last exercise when connected to the DCU 
wireless network (EDUROAM) or the School of Computing's wireless network (if 
available).

3. Use traceroute to determine the route between your machine and a machine on 
your LAN, a machine on another LAN in DCU, a machine outside DCU. (If the 
default UDP option in traceroute does not work try using the ICMP (-I) and TCP 
(-T) options.) What is the IP address of the main DCU router? How do you think 
the DCU network is organised (in terms of LANs, switches, routers)?

```
traceroute -T
```
4. Ping your LAN's broadcast address. **ipconfig** Do you get any responses? Ping the wireless LAN's broadcast address. Do you get any responses?  **Yes, Ping is sent to all on segment** Use **sysctl** to look 
up the kernel's net.ipv4.icmp_echo_ignore_broadcasts setting. Use sysctl to 
look up all net.ipv4 kernel settings.

```
sysctl --all | grep net.ipv4.icmp
```

5. Use the arp command line utility to view the contents of your ARP cache. 
Ping your neighbours machine. List again the contents of your ARP cache. What 
has changed? Look up the corresponding interface vendors here.

```
arp
```

6. Set the Ethernet address of your local router to a bogus value and verify 
you can no longer access the Internet. Reset it to its correct value.

7. Run tcpdump with your network card in promiscuous mode. Output any IP 
traffic not destined for your machine. Ignore multicast and broadcast traffic. 
Is your LAN switched or hubbed? How do you know?

```
tcpdump -i any -f -nn -S -t ip and host not 136.206.18.69

```

8. Use the arp command to clear an entry from your ARP cache. Use tcpdump to 
sniff an ARP request and reply by pinging the entry you just removed. Now ping an unassigned IP address. How many ARP requests does your machine send before 
giving up? (Use filters to ensure you only sniff ARP traffic.)

```
arp -d xxx.xxx.xxx.xxx
```

9. Use tcpdump to sniff the UDP traffic between the client and server you 
created last week. (Again make use of tcpdump's filters.)

10. Use tcpdump to analyse the TCP 3-way handshake between the client and 
server you created last week. (Again make use of tcpdump's filters.)

11. You can use this UDP client to send IP packets of arbitrary size to this 
UDP server. Run client and server on different machines. Monitor traffic with:
• # tcpdump -i any -nn -p -S -t 'ip[6] & 32 = 32 or ip[6:2] & 8191 > 0'
With reference to the IP header definition, can you work out what behaviour 
this filter is looking for? For packets above what size does the behaviour 
occur?

12. Can you cause the same behaviour using TCP? Explain your answer.

13. A client-server system uses a satellite network, with satellites at a 
height of 40,000 km. Calculate the delay and round-trip time. (Speed of light 
is 300,000,000 m/s.)

14. How long (in metres) was a bit in the original 802.3 standard? Assume a 
transmission speed of 10 mbps (mega bits per second) and assume the propagation 
speed in coaxial cable is 2/3 the speed of light in vacuum. (Speed of light in 
vacuum is 300,000,000 m/s.)

15. Use scapy to handcraft and send the packets that establish a connection 
with the server you created last week. (Make sure when using scapy that the 
server you are “connecting to” is on a different machine.) You may need to use 
tcpdump to capture acknowledgment numbers. Use netstat to verify the faked 
connection is in an ESTABLISHED state. This small project requires some work 
but successful completion demonstrates your understanding of TCP's 3-way 
handshake.