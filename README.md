#mssf-network-security

One 2-hour lab per week

- Tuesday, 1700-1900, LG.26
- Completing exercises will occasionally require two lab machines so

work in pairs where applicable

- When necessary (e.g. when root access is required) we may alsouse L1.28 (the taught M.Sc. lab)

### References
[strace](http://khaidoan.wikidot.com/strace)
[20-netstat-commands-for-linux-network-management](http://www.tecmint.com/20-netstat-commands-for-linux-network-management/)