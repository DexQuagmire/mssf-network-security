#CA645 Network Security Lab 3 2014

For many of these exercises you need to work in pairs. Boot one machine into Linux. Boot the other machine into Windows. Download the Fedora Security Spin VM to C:\Temp. Unpack it. Run it in VMWare Workstation/Player. Usernames (and passwords) are student and root. (Note that future users can delete this VM so save anything you want to keep in your own account.)

#1. Get IP

- On Windows use ipconfig to find the address of your DHCP server and all of the information it sends to a client. 
- Do the same on Linux (have a look in the /var/lib/dhcpcd/dhcpcd-eth0.info and /etc/dhclient.conf files).

#2. Spoof IP Address

If you have a laptop, connect it to the wireless network and see if you can transmit a packet with a spoofed, foreign IP address (i.e. one external to DCU) from a machine on one DCU subnet (the lab machine running Fedora Security Spin) to a machine on a different subnet (your laptop on the Eduroam wireless network). 

- To attempt this experiment you can use Fedora's hping2 or hping3 utilities with the -a option to spoof a source IP address. 
- On the target machine you can use tcpdump or wireshark to watch out for the incoming packet with the spoofed source address.
- Does your spoofed packet get through? Are there security implications? How should DCU’s routers be modified to prevent such spoofing?

```
	hping <dest> -a <ip>
```

#3. ARP cache poisoning

- Take a note of your Linux machine's IP address. Take a note of your Linux machine's router's IP and MAC addresses. This is the target machine whose traffic we wish to intercept. Take a note of the MAC address of your Fedora machine. This machine will implement the attack.
- For our man-in-the-middle attack to work we need to turn our Fedora machine into a router. We can do so by disabling its firewall and enabling IP forwarding as follows:

``` bash
	iptables -F
	#BSD
	sysctl -w net.ipv4.ip_forward=1
	#Debian
	echo 1 > /proc/sys/net/ipv4/ip_forward

	Router:
		192.168.192.1

	Victim:
		192.168.192.80

	Attacker:
		192.168.192.74


```
- Run wireshark on Fedora with a filter to capture only HTTP traffic.
- Use the arpspoof command on Fedora to do the poisoning. You need to 
poison two machines: the target and the router. Ensure you only target 
your lab partner's Linux machine. (Note you should keep arpspoof 
running in the background throughout the experiment.)

```
	arpspoof -t 192.168.192.80 192.168.192.1
	arpspoof -t 192.168.192.1 192.168.192.80

```

- Check the victim's ARP cache and verify the router MAC address has become the Fedora machine's MAC address you noted above.

```
	arp -a
```

- Browse some web pages on the victim machine and verify you can see the web traffic in Wireshark on the Fedora machine.

```
	curl www.google.com
```

- Repeat the above cache poisoning experiment with arpwatch running to 
see if it detects the attack. You should see arpwatch display messages as it builds a database of known IP ↔ MAC address mappings. It should then display alerts when it notices any changes in those mappings.

```
	#cleanup
	killall arpspoof
```

#4. SSH Tunnelling

- Set up an SSH tunnel from your Linux machine to **student.computing.dcu.ie**. 
- Use the ***redir*** command line tool to have student.computing.dcu.ie forward your web traffic to the School's web proxy **wwwproxy.computing.dcu.ie:8000**. 
- Connect your browser to the tunnel. Visit whatismyipaddress.com to verify your IP address is that of the proxy and that you have successfully implemented SSH tunneling.


#5. [EXPERIMENTAL! UNTESTED!] UDP hole-punching

Your Fedora machine is running a firewall so should drop unsolicited inbound traffic. Run tcpdump or wireshark on Fedora watching out for UDP traffic from you Linux machine. Below Fedora is the server and your Linux machine is the client.

- Use netcat to listen for UDP traffic on port 5555 on the server:
$ nc -u -l 5555
- Try to talk to the server (assuming it is at 192.168.1.55) from port 
3333 on the client:
$ nc -u -p 3333 192.168.1.55 5555
- The above traffic is blocked by the server’s firewall.
- Kill the netcat listener on the server.
- Attempt to talk to the client (assuming it is at 192.168.1.33) from the server:
$ nc -u -p 5555 192.168.1.33 3333
- The above traffic is blocked by the client’s firewall.
- Repeat the first step above.
- Repeat the second step above.
- You should see that this time traffic is successfully delivered to the 
server because the server’s firewall is now expecting traffic from the 
client. The two machines are communicating without our having modified any firewall settings.