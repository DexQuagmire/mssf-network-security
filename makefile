victim:
	vagrant box add precise64 http://files.vagrantup.com/precise64_vmware.box
	
	vagrant up --provider=vmware_fusion

victim2:
	vagrant box add saucy-vmware http://brennovich.s3.amazonaws.com/saucy64_vmware_fusion.box

	vagrant up --provider=vmware_fusion
	
cleanup:
	vagrant destroy
