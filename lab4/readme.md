DNS Lookup Utility

        dig +multiline soa computing.dcu.ie

        dig +trace www.rte.ie


#CA645 Network Security Lab 4 2014

For many of these exercises you need to work in pairs. Boot one machine into 
Linux. Boot the other machine into Windows. Download the Fedora Security Spin 
VM to C:\Temp. Unpack it. Run it in VMWare Workstation/Player. Usernames (and 
passwords) are student and root. (Note that future users can delete this VM 
so save anything you want to keep in your own account.)

##A. DNS

1.Use dig to lookup the IP address of www.rte.ie.

```	
dig www.rte.ie
```

2.Run the same lookup several times. What changes from one run to the next?

```
id
```

3.Explain each of the following in the DNS response header:
opcode
status
id
qr
rd
ra
QUERY
ANSWER
AUTHORITY
ADDITIONAL

4.Is the answer you receive authoritative?

	no

5.How do you know?


```
No ‘aa’
```

6.Use dig to lookup the name server(s) responsible for www.rte.ie.

```
dig NS www.rte.ie
; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> NS www.rte.ie
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23003
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.rte.ie.			IN	NS

;; AUTHORITY SECTION:
rte.ie.			865	IN	SOA	ns-1264.awsdns-30.org. hostmaster.rte.ie. 1 7200 900 1209600 86400

;; Query time: 6 msec
;; SERVER: 136.206.11.45#53(136.206.11.45)
;; WHEN: Tue Mar 04 17:57:07 GMT 2014
;; MSG SIZE  rcvd: 107
```

7.Use dig to get an authoritative answer.

8.Which flag indicates the answer you receive is authoritative?

	aa

9.Query the RTE name server for a non-existent host in the RTE domain. What 
status is returned in the DNS header?

```
dig wwwDSDS.rte.ie
; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> wwwDSDS.rte.ie
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 62430
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1
```

10.Query the RTE name server for www.google.ie. Does it give you an answer? 
What status is returned in the DNS header? Does the RTE name server support 
recursive queries (i.e. is it willing to perform lookups on domains for which 
it is non-authoritative)?

```
dig @ns-448.awsdns-56.com www.google.com
; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> @ns-448.awsdns-56.com www.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 58849
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;www.google.com.			IN	A

;; Query time: 27 msec
;; SERVER: 205.251.193.192#53(205.251.193.192)
;; WHEN: Tue Mar 04 18:12:27 GMT 2014
;; MSG SIZE  rcvd: 32
```

11.Query the name server 8.8.8.8 for www.rte.ie. Does it give you an answer? 
Who runs the 8.8.8.8 name server?

12.Use dig +trace to trace the lookup of www.rte.ie. 

	dig +trace rte.ie

13.List the name servers for the .ie domain.
	
	dig +trace ie

14.Query one of the .ie domain name servers for www.rte.ie. Does it give you 
the answer? What status is returned in the DNS header?
	
	dig @a.iedr.ie rte.ie

15.What does it instead tell you?
	Where to lok up next
 
##B. Network Enumeration

1.Use dig to retrieve the SOA, NS and MX DNS records for the dcu.ie and 
computing.dcu.ie domains.

2.Consult the fixedorbit.com/search.htm to determine the network class of 
the DCU network. Do the same for TCD and UCD. What do all three have in 
common? (Hint: look at the AS number associated with each.)

3.What is this page showing you?

4.Try issuing a zone transfer request to each of DCU's and the School of 
Computing’s name servers. Any success? (If you are unsuccessful here you can 
try some other DNS servers in the Irish educational institutes. Some are 
vulnerable.)

```
dig NS computing.dcu.ie

; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> NS computing.dcu.ie
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47939
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 5

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;computing.dcu.ie.		IN	NS

;; ANSWER SECTION:
computing.dcu.ie.	600	IN	NS	ns3.computing.dcu.ie.

dig @ns3.computing.dcu.ie dcu.ie axfr

; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> @ns3.computing.dcu.ie dcu.ie axfr
```

5.Download and build ghba from here. Use ghba to do a reverse DNS sweep of 
the 136.206.1.0/24 and 136.206.11.0/24 subnets.

```
dig NS dit.ie

; <<>> DiG 9.9.4-P2-RedHat-9.9.4-11.P2.fc20 <<>> NS dit.ie
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48248
;; flags: qr rd ra; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 6

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;dit.ie.				IN	NS

;; ANSWER SECTION:
dit.ie.			2656	IN	NS	hermes.dit.ie.

dig @ns.heanet.ie wit.ie axfr
dig @inet.tinia.ie dit.ie axfr
```

6.Repeat the previous step at home. Do you get similar results? If so, what 
are the security implications? How would you address this issue?

7.Use a Google search to find all the web servers in DCU that have indexing 
enabled. Find anything interesting?
	
	intitle: index.of "parent directory" site: .dcu.ie

8.Use a Google search to find all of the publicly accessible Excel (.xls) 
spreadsheets on DCU's web servers. Do any security-relevant spreadsheets show 
up?
	
	Filedype:xls site:.dcu.ie

9.Use nmap to discover the open web servers running on the 136.206.1.0/24 
subnet. How many are there?
	
	nmap -p80 136.206.11.0-255

10.Do the same for ftp servers, DNS servers, telnet servers etc. Note: Use a 
SYN scan and only scan the ports associated with these applications.

```
http://nmap.org/bennieston-tutorial/
nmap –sS -p80, 21, 23, 57  136.206.11.0-255
```

11.Check whether any of the mail servers you discovered earlier can act as 
an open mail relay. To conduct the test, connect to a mail server from the 
command line:
$ telnet mailhost.computing.dcu.ie 25
...
HELO world
...
HELP...
EHLO world
...
MAIL FROM: joe@happyplace.com
...
RCPT TO: youremailaddress
...
DATA
This is a message to you.
.
The above will succeed in sending a message to yourself from a spoofed e-mail 
address (note how the mail server does not request authentication). If you 
can do the same from outside DCU then the mail server is not securely 
configured and is acting as an “open mail relay”.